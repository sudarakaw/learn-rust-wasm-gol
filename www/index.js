import { Universe, Cell } from 'wasm-gol'
import { memory } from '../pkg/index_bg.wasm'

class FPS {
    constructor() {
        this.fps = document.getElementById('fps')
        this.frames = []
        this.lastFrameTimeStamp = performance.now()
    }

    render() {
        // Convert the delta time since the last frame render into a measure of
        // frames per second.

        let min = Infinity
          , max = -Infinity
          , sum = 0
          , mean = 0

        const now = performance.now()
            , delta = now - this.lastFrameTimeStamp
            , fps = 1 / delta * 1000

        this.lastFrameTimeStamp = now

        // Save only the latest 100 timings.
        this.frames.push(fps)

        if(100 < this.frames.length) {
            this.frames.shift()
        }

        // Find the max, min, and mean of out 100 latest timings.
        for(let i = 0; i < this.frames.length; i++) {
            sum += this.frames[i]
            min = Math.min(this.frames[i], min)
            max = Math.max(this.frames[i], max)
        }

        mean = sum / this.frames.length

        // Render the statistics
        this.fps.textContent = `
Frames per Second:
         latest = ${Math.round(fps)}
avg of last 100 = ${Math.round(mean)}
min of last 100 = ${Math.round(min)}
max of last 100 = ${Math.round(max)}
`.trim()
    }
}

let animationId = null

const CELL_SIZE = 5 // pixels
    , GRID_COLOR = '#ccc'
    , DEAD_COLOR = '#fff'
    , ALIVE_COLOR = '#000'

      // Construct the universe, and get its width and height.
    , universe = Universe.new()
    , width = universe.width()
    , height = universe.height()

      // Give the canvas room for all of our cells and a 1px border
      // around each of them.
    , canvas = document.getElementById('gol-canvas')
    , ctx = canvas.getContext('2d')

    , playPauseButton = document.getElementById('play-pause')

    , fps = new FPS()


    , getIndex = (row, col) => row * width + col

    , play = () => {
        playPauseButton.textContent = '⏸'

        render_loop()
      }

    , pause = () => {
        playPauseButton.textContent = '▶️'

        cancelAnimationFrame(animationId)

        animationId = null
      }

    , isPaused = () => animationId === null

    , render_loop = () => {
        // debugger;

        fps.render()

        draw_grid()
        draw_lines()

        universe.tick()

        animationId = requestAnimationFrame(render_loop)
      }

    , draw_grid = () => {
        ctx.beginPath()
        ctx.strokeStyle = GRID_COLOR

        // Vertical lines.
        for(let i = 0; i <= width; i++) {
            ctx.moveTo(i * (CELL_SIZE + 1) + 1, 0)
            ctx.lineTo(i * (CELL_SIZE + 1) + 1, height * (CELL_SIZE + 1) + 1)
        }

        // Horizontal lines.
        for(let i = 0; i <= height; i++) {
            ctx.moveTo(0                          , i * (CELL_SIZE + 1) + 1)
            ctx.lineTo(width * (CELL_SIZE + 1) + 1, i * (CELL_SIZE + 1) + 1)
        }

        ctx.stroke()
      }

    , draw_lines = () => {
        const cell_ptr = universe.cells()
            , cells = new Uint8Array(memory.buffer, cell_ptr, width * height)

        ctx.beginPath()

        // Dead cells.
        ctx.fillStyle = DEAD_COLOR

        for(let row = 0; row < height; row++) {
            for(let col = 0; col < width; col++) {
                const idx = getIndex(row, col)

                if(cells[idx] !== Cell.Dead) {
                    continue
                }

                ctx.fillRect
                    ( col * (CELL_SIZE + 1) + 1
                    , row * (CELL_SIZE + 1) + 1
                    , CELL_SIZE
                    , CELL_SIZE
                    )
            }
        }

        // Alive cells.
        ctx.fillStyle = ALIVE_COLOR

        for(let row = 0; row < height; row++) {
            for(let col = 0; col < width; col++) {
                const idx = getIndex(row, col)

                if(cells[idx] !== Cell.Alive) {
                    continue
                }

                ctx.fillRect
                    ( col * (CELL_SIZE + 1) + 1
                    , row * (CELL_SIZE + 1) + 1
                    , CELL_SIZE
                    , CELL_SIZE
                    )
            }
        }

        ctx.stroke()
      }

canvas.width = (CELL_SIZE + 1) * width + 1
canvas.height = (CELL_SIZE + 1) * height + 1

canvas.addEventListener('click', ev => {
    const boundingRect = canvas.getBoundingClientRect()
        , scaleX = canvas.width / boundingRect.width
        , scaleY = canvas.height / boundingRect.height
        , canvasLeft = (ev.clientX - boundingRect.left) * scaleX
        , canvasTop = (ev.clientY - boundingRect.top) * scaleY
        , row = Math.min(Math.floor(canvasTop / (CELL_SIZE + 1)), height - 1)
        , col = Math.min(Math.floor(canvasLeft / (CELL_SIZE + 1)), width - 1)

    universe.toggle_cell(row, col)

    draw_grid()
    draw_lines()
})

playPauseButton.addEventListener('click', _ => {
    if(isPaused()) {
        play()
    }
    else {
        pause()
    }
})

// This used to be `requestAnimationFrame(render_loop)`.
play()
