const CopyWebpackPlugin = require("copy-webpack-plugin");
const WasmPackPlugin = require('@wasm-tool/wasm-pack-plugin')
const path = require('path');

module.exports = {
  'context': path.resolve(__dirname, "www"),
  entry: { 'index.js': "./index.js"},
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "[name]",
  },
  mode: "development",
  plugins: [
    new CopyWebpackPlugin({ 'patterns': ['index.html'] }),
    new WasmPackPlugin({ 'crateDirectory': __dirname
      , 'extraArgs': '--no-typescript'
    })
  ],
  'experiments': {
    'asyncWebAssembly': true
  }
  , 'devServer':
    {
       'host': '127.0.0.1'
      , 'port': 5000
    }
};
